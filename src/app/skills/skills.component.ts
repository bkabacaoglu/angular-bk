import { Component, OnInit } from '@angular/core';
import { Skill } from '../shared/skill';
import { SkillService } from '../services/skill.service';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})


export class SkillsComponent implements OnInit {

  skills:Skill[];
  errMess:string;

  constructor(private skillService: SkillService) { }

  ngOnInit() {
    this.skillService.getSkills().subscribe(skills =>  this.skills = skills,
      errMess  => this.errMess  = <any>errMess);
  }

}
