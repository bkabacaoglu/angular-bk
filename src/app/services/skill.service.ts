import { Injectable } from '@angular/core';
import { Skill } from '../shared/skill';
import { SKILLS } from '../shared/skills';

import { of ,Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor() { }

  getSkills(): Observable<Skill[]> {
    return of(SKILLS);
  }
}
