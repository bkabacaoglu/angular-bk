export class ContactData {
    firstname: string;
    lastname: string;
    email: string;
    message: string;
};